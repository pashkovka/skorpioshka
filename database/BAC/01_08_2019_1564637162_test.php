<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

class Test_my implements \Core\Migrations\Migration {
    public function up()
    {
        Capsule::schema()->create('test_my', function (Blueprint $table){
        $table->increments('id');
        $table->string('test');
        $table->timestamps();
});
    }
    
    
    public function down()
    {
        Capsule::schema()->dropIfExists('test_my');
    }
}